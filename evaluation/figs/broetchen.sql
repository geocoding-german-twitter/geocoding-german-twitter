CREATE TABLE broetchen_nuts3_regex AS
        SELECT nuts_3, (REGEXP_MATCHES(Text::varchar, '((?:(^|\W))brötchen(?:($|\W)))|((?:(^|\W))broetchen(?:($|\W)))|((?:(^|\W))semmel(?:($|\W)))|((?:(^|\W))semmerl(?:($|\W)))|((?:(^|\W))semmeln(?:($|\W)))|((?:(^|\W))semmerln(?:($|\W)))|((?:(^|\W))weck(?:($|\W)))|((?:(^|\W))wecke(?:($|\W)))|((?:(^|\W))weckle(?:($|\W)))|((?:(^|\W))weckerl(?:($|\W)))|((?:(^|\W))weckerle(?:($|\W)))|((?:(^|\W))schrippe(?:($|\W)))|((?:(^|\W))kipf(?:($|\W)))', 'gi'))
        FROM tweets_geo_3y_nrt_nv
        WHERE nuts_3 IS NOT NULL;

CREATE TABLE broetchen_nuts2_regex AS
        SELECT nuts_2, (REGEXP_MATCHES(Text::varchar, '((?:(^|\W))brötchen(?:($|\W)))|((?:(^|\W))broetchen(?:($|\W)))|((?:(^|\W))semmel(?:($|\W)))|((?:(^|\W))semmerl(?:($|\W)))|((?:(^|\W))semmeln(?:($|\W)))|((?:(^|\W))semmerln(?:($|\W)))|((?:(^|\W))weck(?:($|\W)))|((?:(^|\W))wecke(?:($|\W)))|((?:(^|\W))weckle(?:($|\W)))|((?:(^|\W))weckerl(?:($|\W)))|((?:(^|\W))weckerle(?:($|\W)))|((?:(^|\W))schrippe(?:($|\W)))|((?:(^|\W))kipf(?:($|\W)))', 'gi'))
        FROM tweets_geo_3y_nrt_nv
        WHERE nuts_2 IS NOT NULL;