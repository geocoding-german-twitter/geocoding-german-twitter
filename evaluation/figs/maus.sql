CREATE TABLE MAUs as (SELECT COUNT(DISTINCT user_id), DATE_TRUNC('month', post_date), geo_source FROM
    tweets_geo_3y_nrt_nv GROUP BY
    DATE_TRUNC('month', post_date), geo_source);