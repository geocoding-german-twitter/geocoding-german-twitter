-- regex version

CREATE TEMP TABLE partysupport_temp_regex AS (
     SELECT DISTINCT ON (tweet_id) tweet_id, nuts_2, REGEXP_MATCHES(Text::varchar, '((?:(^|\W))#grün(?:($|\W)))|((?:(^|\W))#gruen(?:($|\W)))|((?:(^|\W))#grüne(?:($|\W)))|((?:^|\W)#gruene(?:$|\W))|((?:^|\W)#diesmalgrün(?:$|\W))|((?:^|\W)#diesmalgruen(?:$|\W))|((?:^|\W)#grünwählen(?:$|\W))|((?:^|\W)#gruenwaehlen(?:$|\W))|((?:^|\W)#bereitweilihresseid(?:$|\W))', 'gi') FROM  
     tweets_geo_3y_nrt_nv  
     WHERE  
     post_date BETWEEN '2021-08-28' AND '2021-09-26'  
);

CREATE TABLE partysupport_regex AS(
    SELECT COUNT(1), nuts_2 FROM
    partysupport_temp_regex GROUP BY
    nuts_2
);
