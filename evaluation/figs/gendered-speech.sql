-- gendered speech regex

CREATE TEMP TABLE gendered_speech_temp_regex AS (
     SELECT DISTINCT ON (user_id) user_id, nuts_3, REGEXP_MATCHES(Text::varchar, '((?:(^|[a-zA-Z]))In(?:($|\W)))|((?:(^|[a-zA-Z]))Innen(?:($|\W)))|((?:(^|[a-zA-Z]))_in(?:($|\W)))|((?:(^|[a-zA-Z]))_innen(?:($|\W)))|((?:(^|[a-zA-Z])):in(?:($|\W)))|((?:(^|[a-zA-Z])):innen(?:($|\W)))|((?:(^|[a-zA-Z]))\*in(?:($|\W)))|((?:(^|[a-zA-Z]))\*innen(?:($|\W)))','') FROM  
     tweets_geo_3y_nrt_nv  
     WHERE  
        nuts_3 IS NOT NULL
     );

CREATE TABLE gendered_speech_ulvl_nuts3_regex AS(
    SELECT COUNT(1), nuts_3 FROM
    gendered_speech_temp_regex GROUP BY
    nuts_3
);
