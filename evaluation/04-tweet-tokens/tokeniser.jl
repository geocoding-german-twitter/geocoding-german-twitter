"""
    tokenise(input::String; transform_to_lower_case = true)

Turns a sentence into tokens. Split happens on spaces, afterwards non-unicode characters are removed. Does not deal with URLs as these are removed by PostgresSQL.
"""
function tokenise(input::String; transform_to_lower_case = true)::Vector{String}

    if transform_to_lower_case == true
        input = lowercase(input)
    end

    input = pre_process(input, true, true) # input, strip_handle, reduce_len

    tb = TokenBuffer(input)

    while !isdone(tb)
        spaces(tb) && continue
        _nonunicodecharacters(tb) ||
        character(tb)
    end

    tb.tokens[tb.tokens .|> length .!= 1]

    return tb.tokens

end

"Removes all non-unicode characters from a token."
function _nonunicodecharacters(tb::TokenBuffer)
    Base.Unicode.category_code(tb[]) != 2 && tb[tb.idx] != '#' || return false
    #cc != 2 && tb[tb.idx] != '#' || return false
    flush!(tb)
    tb.idx += 1
    return true
end