using LibPQ
using DotEnv
using DataFrames
using Dictionaries
using StatsBase
using Distances

DotEnv.config();
conn = LibPQ.Connection(ENV["CONN_STRING"])

full = execute(conn, "SELECT * from vocab_full_notverified") |>
        DataFrame |>
        x -> sort!(x, :frequency, rev = true) |>
        disallowmissing!


nogeo = execute(conn, "SELECT * from vocab_nogeo_notverified") |>
        DataFrame |>
        x -> sort!(x, :frequency, rev = true) |>
        disallowmissing!


geo = execute(conn, "SELECT * from vocab_geo_notverified") |>
        DataFrame |>
        x -> sort!(x, :frequency, rev = true) |>
        disallowmissing!


full.frequency = convert.(Int64,full.frequency)
geo.frequency = convert.(Int64,geo.frequency)
nogeo.frequency = convert.(Int64,nogeo.frequency)

nogeo_geo = innerjoin(nogeo, geo, on = :token, renamecols = "_nogeo" => "_geo")

1 - cosine_dist(nogeo_geo.frequency_nogeo, nogeo_geo.frequency_geo)

A = Set(geo.token)
B = Set(nogeo.token)

(length(intersect(A,B))) / (length(union(A,B)))