using Pkg
Pkg.activate(".")

include("evaluation/04-tweet-tokens/GeocodedContent.jl")


conn = LibPQ.Connection(ENV["CONN_STRING"])

table = "vocab_geo_notverified"
cursor_name = "TextCurs"

# DROP TABLE IF EXISTS $table;

execute(conn,"
    CREATE TABLE $table(
        token VARCHAR,
        frequency Integer
    );
")

LibPQ.execute(conn, "BEGIN;")

# query ="
# DECLARE $cursor_name SCROLL CURSOR FOR
#     SELECT REGEXP_REPLACE(Text::varchar ,'(https*):\\/\\/([[:alnum:]|\\.|\\-|\\/|\\?|=])+',' ','gi') AS Text
#     FROM Tweets
#     WHERE NOT isretweet
#     AND coordinates_longitude IS NOT NULL
#     AND post_date BETWEEN '2018-10-15' AND '2021-10-14';
# "

query ="
DECLARE $cursor_name SCROLL CURSOR FOR
    SELECT REGEXP_REPLACE(Text::varchar ,'(https*):\\/\\/([[:alnum:]|\\.|\\-|\\/|\\?|=])+',' ','gi') AS Text
FROM tweets_geo_3y_nrt_nv
WHERE geo_source IS NOT NULL;
"

# AND coordinates_longitude IS NOT NULL

qp = LibPQ.execute(conn,"
    EXPLAIN
    $query
") |> columntable

# query_expected_rows = 884065424 rows of full with no retweets

query_expected_rows = match(r"rows=\d+", qp[1][1]) |>
x -> convert(String, x.match) |>
x -> match(r"\d+", x) |>
x -> convert(String,x.match) |>
x -> parse(Int, x)

LibPQ.execute(conn, query)

full = execute(conn, "SELECT * from vocab_full_notverified") |>
DataFrame |>
disallowmissing! |>
x -> sort!(x, :frequency, rev = true)

vocab = Dictionary{String,Int32}()
# wordlist = Indices{String}()
wordlist = Indices(full.token)

function main()
    generate_vocabulary(conn, vocab, cursor_name, 2000, 25, query_expected_rows, true, wordlist)
end

main()


LibPQ.load!(
    vocab |> pairs |> rowtable,
    conn,
    "INSERT INTO $table (token, frequency) VALUES (\$1, \$2);",
)

LibPQ.execute(conn, "COMMIT;")

close(conn)
