
"""
    parse_tweets!(tweets::Vector{String}, vocab::Dictionary{String,Int32})

Tokenises a vector of sentences (e.g. tweets) and updates an already initialized Dictionary with the respective counts for each extracted token.

For details on how the tokens are create from the sentences, see `tokenise`.
"""
function parse_tweets!(tweets::Vector{String}, vocab::Dictionary{String,Int32})

        words = collect(token for tweet in tweets for token in tokenise(tweet))
        
        for word in words
                word |> word -> set!(vocab, word, get!(vocab, word, 0) + 1)
        end
end

function parse_tweets!(tweets::Vector{String}, vocab::Dictionary{String,Int32}, wordlist::Indices{String})

    words = collect(token for tweet in tweets for token in tokenise(tweet))

    for word in words
        (word in wordlist) ? (word |> word -> set!(vocab, word, get!(vocab, word, 0) + 1)) : continue
    end

end


"""
    generate_vocabulary(conn::LibPQ.Connection, vocab::Dictionary{String,Int32}, cursor_name::String, batch_size::Int64, min_word_count::Int, expected_rows::Int)

Generates a vocabulary (i.e. token => count mapping) from a given LibPQ connection for which a cursor with name `cursor_name` has already been created.
Fetches forward `batch_size` rows from the cursor on every call to the database. Excludes tokens which appear less than `min_word_count` times in the data.
`expected_rows` has to be provide as an (estimated) number of total rows so that rare tokens can be removed on a regular basis.
"""
function generate_vocabulary(conn::LibPQ.Connection, vocab::Dictionary{String,Int32}, cursor_name::String, batch_size::Int64, min_word_count::Int, expected_rows::Int,
    prune_by_wordlist::Bool, wordlist::Indices{String})

    prune_every = round.(Int,expected_rows / min_word_count-1)

    prune_thresholds = (prune_every:prune_every:prune_every * min_word_count-2) |> collect

    prune_thresholds = _adjust_thresholds(prune_thresholds, batch_size)

    @show prune_thresholds

    i = 0

    while true

        i += batch_size

        if (i in prune_thresholds && prune_by_wordlist == false)
            println("Pruning on iteration $i.")
            filter!(>(1), vocab)
        end

        result = LibPQ.execute(conn, "FETCH FORWARD $batch_size FROM $cursor_name;")

        if prune_by_wordlist

            try
                _result_to_string(result) |> x -> parse_tweets!(x, vocab, wordlist)
            catch
                @warn "Possible bounds error."
            end

        else

            try
                _result_to_string(result) |> x -> parse_tweets!(x, vocab)
            catch
                @warn "Possible bounds error."
            end

        end

        if (num_affected_rows(result) < batch_size && prune_by_wordlist == false)
            println("Done. Returning vocabulary.")
            # last prune before return
            filter!(>(min_word_count-1), vocab)
            break
        elseif num_affected_rows(result) < batch_size
            println("Done. Returning vocabulary.")
            break
        end
       
    end

    return vocab

end

"Turn `LibPQ.Result` into a vector of strings."
function _result_to_string(res::LibPQ.Result{false})::Vector{String}
        out = Vector{String}()
        for i in 1:length(res)
            push!(out, res[i,1])
        end
        return out
end

"Adjust the thresholds at which the vocabulary gets pruned to be a multiple of the batch_size (otherwise the iteration on which pruning is due might get skipped)."
function _adjust_thresholds(thresholds, batch_size)
    rem.(thresholds, batch_size) |> x -> thresholds - x
end