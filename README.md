# Geocoding German Twitter

Code to reproduce geocoding and analysis in Nguyen et al. (2022) *Efficient and reliable geocoding of German Twitter data to enable spatial data linkage to official statistics and other data sources*
