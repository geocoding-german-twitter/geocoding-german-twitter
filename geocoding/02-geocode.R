#!/usr/bin/env Rscript

args <- commandArgs(TRUE) |>
  as.integer(args) |>
  Filter(f = Negate(is.na))

stopifnot(length(args) %in% 1:2)

if (length(args) == 1) {
  cat("Last row not specified. End at the nearest 500000.\n")
  args[2] <- ceiling((args[1] + 1) / 500000) * 500000
}

offset <- args[1]
last_row <- args[2]

cat("OFFSET", offset, "\n")
cat("LAST ROW", last_row, "\n")

library(dplyr) |> suppressPackageStartupMessages()
library(tibble)
library(DBI)
library(httr2)

twitter_db <- dbConnect(
  RPostgres::Postgres(),
  dbname = "twitter",
  host = Sys.getenv("TWITTER_DB_HOST"),
  port = Sys.getenv("TWITTER_DB_PORT"),
  user = Sys.getenv("TWITTER_DB_USER")
)

locations_norm <- dbGetQuery(twitter_db, glue::glue("
  SELECT id, location_norm
  FROM locations_norm
  WHERE id > {offset} AND id <= {last_row}
  ORDER BY id;
"))

base_req <- request("http://john.sozput.soz.uni-bielefeld.de:8080/search")

geocode <- function(q, base_req) {
  if (!length(q)) return(NULL)

  stopifnot(length(q) == 1L)

  json <- base_req |>
    req_url_query(
      q = q,
      addressdetails = 1,
      limit = 1
    ) |>
    req_retry(max_tries = 5) |>
    req_perform() |>
    resp_body_json()

  if (!length(json) || !isTRUE(json[[1]]$address$country_code == "de")) {
    return(NULL)
  }

  as_tibble_row(c(
    list(location_norm = q),
    json[[1]][names(json[[1]]) %in% c(
      "place_id",
      "osm_type", "osm_id",
      "place_rank",
      "category", "type",
      "importance"
    )]
  ))
}

geocoded <- tibble(
  id = integer(0),
  location_norm = character(0),
  place_id = integer(0),
  osm_type = character(0),
  osm_id = integer(0),
  place_rank = integer(0),
  category = character(0),
  type = character(0),
  importance = numeric(0)
)

errors <- tibble(
  row = integer(0),
  location_norm = character(0)
)

for (id in locations_norm$id) {
  if (id %% 50 == 1) {
    cat(id, " ")

    dbAppendTable(twitter_db, "geocoded_locations", geocoded)

    geocoded <- head(geocoded, 0)
  }

  q <- locations_norm$location_norm[locations_norm$id == id]

  res <- try(geocode(q = q, base_req = base_req))

  if (inherits(res, "try-error")) {
    errors <- errors |>
      add_row(tibble_row(row = id, location_norm = q))
  } else {
    res$id <- id
    geocoded <- bind_rows(geocoded, res)
  }
}

dbAppendTable(twitter_db, "geocoded_locations", geocoded)

dbDisconnect(twitter_db)

if (nrow(errors) > 0) {
  saveRDS(errors, glue::glue("geocoding-errors-{offset}-{last_row}.rds"))
}
