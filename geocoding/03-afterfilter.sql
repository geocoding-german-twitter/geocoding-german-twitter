CREATE TABLE geocoded_filtered AS
SELECT *
FROM geocoded_locations
-- All locations containing Hangul characters are crap
WHERE NOT location_norm ~ '[\u3131-\u3163\uac00-\ud7a3]'
  AND (

    -- STATES
    (place_rank = 8
      AND NOT osm_id IN (3394111, 1602896793)
      AND (-- MV
           NOT (osm_id = 28322 AND NOT location_norm ~ '[Mm]([VvEe])') AND
           -- SH
           NOT (osm_id = 51529 AND NOT location_norm ~ '[Ss][Cc]?[Hh]') AND
           -- Thüringen
           NOT (osm_id = 62366 AND NOT location_norm ~ '[Tt][Hh][UuÜü]') AND
           -- Berlin
           NOT (osm_id = 62422 AND location_norm ~ '[Bb][Ee][^A-Za-z]') AND
           -- Brandenburg
           NOT (osm_id = 62504 AND NOT location_norm ~ '[Bb][Rr]') AND
           -- Sachsen-Anhalt
           NOT (osm_id = 62607 AND NOT location_norm ~ '((^|\W)[Ss][Tt]($|\W))|([Ll][Tt])') AND
           -- Hessen
           NOT (osm_id = 62650 AND NOT location_norm ~ '([Hh][Ee]|[Aa][Ss])([Ss]|.*[DdGg][Ee])') AND
           -- Niedersachsen: Exclude 'Inferior' without 'Saxonia' and 'Nie'
           NOT (osm_id = 62771 AND location_norm ~ '(?<![Xx][Oo].+)[Ff][Ee][Rr][Ii]|^[Nn][Ii][Ee][^A-Za-z]*$') AND
           -- Hamburg
           NOT (osm_id = 62782 AND location_norm ~ '[Gg][Aa][Mm](?![Bb])') OR
           -- contains coordinates
           location_norm ~ '\d°\d')) OR

    -- COUNTIES
    (place_rank = 10
      -- Only strings that have at least 3 Latin, Umlaut or Cyrillic chars together
      AND location_norm ~ '[A-Za-z\u00c4\u00e4\u00d6\u00f6\u0400\u00dc\u00fc\u00df-\u04FF]{3}'
      -- Schweiz is not Sächsische Schweiz
      AND NOT (osm_id = 202443898 AND location_norm NOT LIKE 'Sächsische Schweiz%')) OR

    (place_rank = 12
      AND NOT location_norm ~ '^[^A-Za-z]*[Dd][Ee]([Uu][Tt][Ss][Cc][Hh][Ll][Aa][Nn][Dd])?[^A-Za-z]*$'
      AND NOT location_norm ~ '^[^A-Za-z]*[Gg][Ee][Rr]([Mm][Aa][Nn][Yy])?[^A-Za-z]*$'
      -- 'lin' or 'lyin' is not Munich
      AND NOT (osm_id = 62428 AND location_norm ~ '^[^A-Za-z]*[Ll]')
      -- 'hm' is not Hamm
      AND NOT (osm_id = 62499 AND location_norm ~ '[Hh][Mm]')
      -- 'bon' is not Bonn
      AND NOT (osm_id = 62508 AND location_norm ~ '[Bb][Oo][^Nn]{2}')
      -- 'monster' is not Münster
      AND NOT (osm_id = 62591 AND location_norm ~ '[Mm][Oo][Nn][Ss][Tt][Ee][Rr]')
      -- 'ina' is not Jena
      AND NOT (osm_id = 62693 AND location_norm ~ '[Ii][Nn][Aa]')
      -- 'land of the free'
      AND NOT location_norm ~ '[Ff][Rr][Ee]{2}') OR

    -- CITIES, MUNICIPALITIES
    (place_rank = 14
      AND NOT location_norm ~ '[Ee]lden [Rr]ing') OR

    (place_rank = 16
      -- meaningless 'land'
      AND NOT location_norm ~ '^(.+[^(er)] )?[Ll][Aa][Nn][Dd]( ([Uu][Nn]|[Oo]|[Dd][Ee]).+)?$'
      AND NOT osm_id IN (448762, 454616, 540449, 1243991, 537205, 539706, 1107767)
      -- Euerdorf, Fensdorf Niederdorf, Osdorf, Breddorf, Rangsdorf, Damsdorf,
      -- Ottendorf, Herdorf, Ehndorf, Hersdorf, Gindorf
      AND NOT (osm_id IN (383697, 421920, 225487, 548490, 1022857, 162462, 411624,
                          913784, 421958, 553602, 1108078, 1140556) AND
               NOT location_norm ~ '[Dd][Oo][Rr][Ff]')
      -- Lupburg, Angelburg, Neuenburg am Rhein, Jesteburg, Ladenburg, Moosburg, Bedburg,
      -- Burg (Dithmarschen), Neubrandenburg, Henstedt-Ulzburg, Lüneburg, Velburg
      AND NOT (osm_id IN (1028818, 225487, 270514, 280522, 454172, 29996, 168465,
                          943896, 62705, 442762, 2420744, 1028815) AND
               NOT location_norm ~ '[Bb][Uu][Rr][Gg]')
      -- Hepberg, Mönkeberg, Sulzberg, Stolberg (\ Rheinland), Friedberg, Annaberg-Buchholz
      AND NOT (osm_id IN (943552, 299456, 957730, 157996, 393689, 536625) AND
               NOT location_norm ~ '([Bb][Ee][Rr][Gg]|[Mm][Oo][Uu][Nn])')
      -- Hartheim, Müllheim, Sinsheim, Münster-Sarmsheim
      AND NOT (osm_id IN (1959389, 311922, 453372, 1239648) AND
               NOT location_norm ~ '[Hh][Ee][Ii][Mm]')
      -- Deinstedt, Itzstedt
      AND NOT (osm_id IN (1079019, 444323) AND
               NOT location_norm LIKE '%tedt%')
      -- Soos
      AND NOT (osm_id = 65325 AND NOT location_norm LIKE '%oos%')
      -- Aull
      AND NOT (osm_id = 537134 AND NOT location_norm LIKE '%ull%')
      -- Theres
      AND NOT (osm_id = 387769 AND NOT location_norm LIKE '%heres%')
      -- Zapel
      AND NOT (osm_id = 449926 AND NOT location_norm LIKE '%apel%')
      -- Hebertshausen
      AND NOT (osm_id = 934469 AND NOT location_norm LIKE '%ebert%')
      -- Neunkirchen am Sand
      AND NOT (osm_id = 1017484 AND NOT location_norm LIKE '%kir%')
      -- Tiste
      AND NOT (osm_id = 1079023 AND NOT location_norm LIKE '%iste%')
      -- Grove
      AND NOT (osm_id = 1467347 AND NOT location_norm LIKE '%rov%')
      -- Rehden
      AND NOT (osm_id = 1588114 AND NOT location_norm LIKE '%ehden%')
      -- Loose
      AND NOT (osm_id = 548620 AND NOT location_norm LIKE '%oos%')
      -- Helse
      AND NOT (osm_id = 935697 AND NOT location_norm LIKE '%els%')
      -- Kieve
      AND NOT (osm_id = 1424156 AND NOT location_norm LIKE '%iev%')
      -- Nideggen
      AND NOT (osm_id = 163312 AND NOT location_norm LIKE '%deg%')
      -- Buch
      AND NOT (osm_id = 540447 AND NOT location_norm LIKE '%uch%')
      -- Friesoythe
      AND NOT (osm_id = 1109689 AND NOT location_norm LIKE '%riesoy%')
      -- Gransee
      AND NOT (osm_id = 1310097 AND NOT location_norm LIKE '%see%')
      -- Bad Liebenwerda
      AND NOT (osm_id = 1328907 AND NOT location_norm LIKE '%ieben%')
      -- Bitterfeld-Wolfen
      AND NOT (osm_id = 1329611 AND NOT location_norm LIKE '%itter%')
      -- Wendisch Rietz
      AND NOT (osm_id = 1384800 AND NOT location_norm LIKE '%iet%')
      -- Langenhorn
      AND NOT (osm_id = 1416813 AND NOT location_norm LIKE '%angen%')
      -- Neukalen
      AND NOT (osm_id = 1427275 AND NOT location_norm LIKE '%euka%')
      -- Jüchen
      AND NOT (osm_id = 168486 AND NOT location_norm LIKE '%chen%')
      -- Zingst
      AND NOT (osm_id = 374275 AND NOT location_norm LIKE '%ingst%')
      -- Dinkelscherben
      AND NOT (osm_id = 452583 AND NOT location_norm LIKE '%inkel%')
      -- Maisach
      AND NOT (osm_id = 933500 AND NOT location_norm LIKE '%sach%')
      -- Wees
      AND NOT (osm_id = 1149283 AND NOT location_norm LIKE '%es%')
      -- Eime
      AND NOT (osm_id = 1149283 AND NOT location_norm ~ '[Ee]ime')
      -- Apen
      AND NOT (osm_id = 1187347 AND NOT location_norm ~ '[Aa]pen')
      -- Riede
      AND NOT (osm_id = 1182271 AND NOT location_norm LIKE '%ied%')
      -- Prien
      AND NOT (osm_id = 2186932 AND NOT location_norm LIKE '%rien%')
      -- Laage
      AND NOT (osm_id = 9682019 AND NOT location_norm LIKE '%aag%')
      -- Oer-Erkenschwick
      AND NOT (osm_id = 63502 AND NOT location_norm LIKE '%er%')
      -- Lemgo
      AND NOT (osm_id = 142624 AND NOT location_norm LIKE '%emg%')
      -- Rees
      AND NOT (osm_id = 167944 AND NOT location_norm LIKE '%ees%')
      -- Emmerich
      AND NOT (osm_id = 167945 AND NOT location_norm LIKE '%mer%')
      -- Laboe
      AND NOT (osm_id = 288261 AND NOT location_norm LIKE '%oe%')
      -- Eichen
      AND NOT (osm_id = 415717 AND location_norm ~ 'asa|ouse')
      -- Königstein
      AND NOT (osm_id = 418253 AND NOT location_norm ~ '[Ss][Tt][Ee][Ii][Nn]')
      -- Dahme/Mark
      AND NOT (osm_id = 422441 AND NOT location_norm LIKE '%ahm%')
      -- Kehl
      AND NOT (osm_id = 452979 AND NOT location_norm LIKE '%ehl%')
      -- Aying
      AND NOT (osm_id = 539953 AND NOT location_norm LIKE '%ying%')
      -- Riesa
      AND NOT (osm_id = 549800 AND NOT location_norm LIKE '%ies%')
      -- Pähl
      AND NOT (osm_id = 936870 AND NOT location_norm LIKE '%hl%')
      -- Gardelegen
      AND NOT (osm_id = 1285598 AND NOT location_norm LIKE '%egen%')
      -- Guben
      AND NOT (osm_id = 1351322 AND NOT location_norm LIKE '%uben%')
      -- Demmin
      AND NOT (osm_id = 1427221 AND NOT location_norm LIKE '%mmin%')
      -- Wedel
      AND NOT (osm_id = 3370039 AND NOT location_norm LIKE '%ede%')
      -- Moers
      AND NOT (osm_id = 58623 AND NOT location_norm ~ '[Mm][Oo][Ee][Rr]')
      -- Rheine
      AND NOT (osm_id = 155707 AND NOT location_norm ~ '[Rr][Hh][Ee][Ii]')
      -- Düren
      AND NOT (osm_id = 163046 AND NOT location_norm ~ '[Dd]([Üü]|[Uu])')
      -- Nois is not Noisas (Neuss)
      AND NOT (osm_id = 163307 AND location_norm ~ 'nois([^a]|$)')
      -- Leun
      AND NOT (osm_id = 215981 AND NOT location_norm ~ '[Eu][Uu]')
      -- Nohn
      AND NOT (osm_id = 331057 AND NOT location_norm ~ '[Oo][Hh]')
      -- Barby
      AND NOT (osm_id = 389267 AND NOT location_norm LIKE '%by%')
      -- Nahe
      AND NOT (osm_id = 422706 AND NOT location_norm ~ '[Nn][Aa][Hh][Ee](,? [Dd][Ee].*)?$')
      -- Wanna
      AND NOT (osm_id = 1180093 AND NOT location_norm ~ '[Ww][Aa][Nn]{2}[Aa](,? (Dd|Gg)[Ee].*)?$')
      -- Blender
      AND NOT (osm_id = 1182293 AND NOT location_norm ~ '^[Bb]lender(,? (Dd)[Ee].*)?$')
      -- Vaale
      AND NOT (osm_id = 448608 AND NOT location_norm LIKE '%aale%')
      -- Hardt
      AND NOT (osm_id = 451707 AND NOT location_norm LIKE '%rdt%')
      -- Holle
      AND NOT (osm_id = 1168891 AND NOT location_norm LIKE '%oll%')
      -- Bühl
      AND NOT (osm_id = 1188919 AND NOT location_norm ~ '[Bb]([Üü]|[Uu])')
      -- Rehborn
      AND NOT (osm_id = 1244058 AND NOT location_norm LIKE '%eh%')
      -- Celle
      AND NOT (osm_id = 1797918 AND NOT location_norm ~ '[Cc][Ee]')
      -- Gotha
      AND NOT (osm_id = 2818690 AND NOT location_norm ~ '[Gg][Oo][Tt][Hh][Aa]')
      -- Ölsen
      AND NOT (osm_id = 418879 AND NOT location_norm ~ '[Öö]|[Oo]e')
      -- Aglasterhausen/Hause
      AND NOT (osm_id = 403747 AND location_norm ~ ' [Hh]')
      -- Lage
      AND NOT (osm_id = 142650 AND NOT location_norm LIKE '%age%')
      -- Bad Tölz
      AND NOT (osm_id = 142650 AND NOT location_norm ~ '(ö|oe)l')
      -- Train
      AND NOT (osm_id = 956657 AND NOT location_norm ~ '^[Tt][Rr]')
      -- Alfstedt
      AND NOT (osm_id = 1079020 AND NOT location_norm LIKE '%lfs%')
      -- Nieste
      AND NOT (osm_id = 1115034 AND NOT location_norm LIKE '%iest%')
      -- Kist
      AND NOT (osm_id = 163747 AND NOT location_norm ~ '[Kk][Ii]')) OR

    -- ISLANDS
    (place_rank = 17
      -- 'for' is not Föhr
      AND NOT (osm_id = 3352541 AND NOT location_norm LIKE '%öhr%')
      -- 'just' is not Juist
      AND NOT (osm_id = 3355571 AND location_norm LIKE '%ust%')
      -- Koos and Greifswalder Oie: only false positives
      AND NOT osm_id IN (3790231, 3790746)) OR

    -- TOWNS
    (place_rank = 18
      AND NOT location_norm ~ '^[^A-Za-z]*[Dd][Ee]([Uu][Tt][Ss][Cc][Hh][Ll][Aa][Nn][Dd])?[^A-Za-z]*$'
      AND NOT location_norm ~ '^[^A-Za-z]*[Gg][Ee][Rr]([Mm][Aa][Nn][Yy])?[^A-Za-z]*$'
      -- Most peaks are crap
      AND NOT type = 'peak'
      -- Ambiguous phrases
      AND NOT osm_id IN (16347, 75732, 318383, 318409, 379746, 406638, 445754,
                         1111003, 1130801, 1325206, 1395306, 1395369, 1395406,
                         1806581, 2027764, 2309352, 2313918, 2346038, 2912762,
                         3245621, 3388642, 3696884, 3968641, 4508754, 5201303,
                         5213209, 5623499, 6315486, 7076840, 7076841, 7076844,
                         7995671, 8356776, 9432191, 9849641, 10507209, 10966429,
                         11070478, 11820356, 13130198, 13132693, 13132801, 13694466,
                         412367)
      AND NOT osm_id BETWEEN 8288157 AND 8311011
      AND NOT osm_id BETWEEN 9851226 AND 9860268
      AND NOT (osm_id BETWEEN 11042423 AND 11042429 AND NOT location_norm ~ '[Ff]')
      AND NOT (osm_id = 54373 AND NOT location_norm LIKE '%hakir%')
      AND NOT (osm_id = 54376 AND NOT location_norm LIKE '%adern%')
      AND NOT (osm_id = 54388 AND NOT location_norm LIKE '%aim%')
      AND NOT (osm_id = 55734 AND NOT location_norm ~ 'ehlen|EHLEN')
      AND NOT (osm_id = 56391 AND NOT location_norm LIKE '%ehel%')
      AND NOT (osm_id = 56395 AND NOT location_norm LIKE '%arlach%')
      AND NOT (osm_id = 123924 AND NOT location_norm ~ 'he|hi')
      AND NOT (osm_id = 318372 AND NOT location_norm LIKE '%hr%')
      AND NOT (osm_id = 390393 AND NOT location_norm ~ 'aa|arr')
      AND NOT (osm_id = 409090 AND NOT location_norm LIKE '%ieg%')
      AND NOT (osm_id = 1460411 AND NOT location_norm LIKE '%ehen%')
      AND NOT (osm_id = 2255753 AND NOT location_norm LIKE '%ens%')
      AND NOT (osm_id = 2255753 AND location_norm LIKE '%shy%')
      AND NOT (osm_id = 2255753 AND NOT location_norm LIKE '%orz%')
      AND NOT (osm_id = 3388634 AND NOT location_norm LIKE '%ord%')
      AND NOT (osm_id = 5744413 AND NOT location_norm LIKE '%chern%')
      AND NOT (osm_id = 8821182 AND NOT location_norm LIKE '%öhl%')
      AND NOT (osm_id = 9849593 AND NOT location_norm LIKE '%aas%')
      AND NOT (osm_id = 3388644 AND NOT location_norm LIKE '%olf%')
      AND NOT (osm_id = 3286566 AND NOT location_norm LIKE '%eck%')
      AND NOT (osm_id = 387502 AND NOT location_norm LIKE 'S%')
      AND NOT (osm_id = 370068 AND NOT location_norm ~ '[Mm][Ss]')
      AND NOT (osm_id = 16343 AND NOT location_norm ~ '[Ss][Pp]')
      AND NOT (osm_id = 52747 AND NOT location_norm LIKE '%ruder%')
      AND NOT (osm_id = 10764427 AND location_norm ~ '[Öö]')) OR

    -- VILLAGES, SUBURBS
    (place_rank = 19
      AND NOT location_norm ~ '^[^A-Za-z]*[Dd][Ee]([Uu][Tt][Ss][Cc][Hh][Ll][Aa][Nn][Dd])?[^A-Za-z]*$'
      AND NOT location_norm ~ '^[^A-Za-z]*[Gg][Ee][Rr]([Mm][Aa][Nn][Yy])?[^A-Za-z]*$'
      -- crap
      AND NOT osm_id IN (59867190, 91239834, 111697960, 117825904, 144119434,
                         169544424, 240093040, 245449506, 269973648, 276653103,
                         279181945, 282930011, 293979674, 296878445, 298552220,
                         300983083, 309540514, 311840009, 313801024, 317935031,
                         319313567, 32132216, 330043706, 336047606, 340324070, 343682812,
                         360957345, 373927939, 388087540, 388434446, 420307581,
                         502904640, 502950301, 503266158, 566145237, 578067317,
                         610474800, 673376261, 703964138, 765892927, 779429350,
                         822311120, 843759224, 858077867, 932241784, 959508473,
                         1017117782, 1117653099, 1430615620, 1457660865, 1507074844,
                         1612822137, 1651567226, 1657720599, 1700564762, 1700567012,
                         1755799989, 1861339345, 2041704839, 2109585430, 2357453391,
                         2445664642, 2568677234, 2629674789, 2824755336, 3992822596,
                         4571450660, 6183246968, 9111464894, 271774, 385461, 407981,
                         9639261, 26435496, 41964368, 50220192, 665441322, 796315621,
                         2139951265, 557149265, 379706353)
      AND NOT osm_id BETWEEN 412112 AND 414218
      -- 'marlie' is not Marli
      AND NOT (osm_id = 52762828 AND location_norm LIKE '%lie%')
      -- Oy
      AND NOT (osm_id = 60455426 AND NOT location_norm ~ '[Oo]y')
      -- 'see ya' is not See
      AND NOT (osm_id = 66492141 AND location_norm LIKE '%ya%')
      -- 'viσlєτ.♡' is not Wieslet
      AND NOT (osm_id = 240104866 AND NOT location_norm LIKE '%ieslet%')
      -- 'piss' is not Pißdorf
      AND NOT (osm_id = 257797323 AND NOT location_norm LIKE '%dorf%')
      -- Vagen
      AND NOT (osm_id = 268266231 AND NOT location_norm LIKE '%agen%')
      -- Weng
      AND NOT (osm_id = 271399464 AND NOT location_norm LIKE '%eng%')
      -- 'leia' is not Leiha
      AND NOT (osm_id = 272494005 AND NOT location_norm LIKE '%eiha%')
      -- Badorf, Baalsdorf
      AND NOT (osm_id IN (316809496, 1680861567) AND
               NOT location_norm LIKE '%dorf%')
      -- Weha
      AND NOT (osm_id = 330228418 AND NOT location_norm LIKE '%eha%')
      -- Zürch
      AND NOT (osm_id = 403298222 AND NOT location_norm LIKE '%ürch%')
      -- Leeden
      AND NOT (osm_id = 417976079 AND NOT location_norm LIKE '%eeden%')
      -- 'loey' is not Loy
      AND NOT (osm_id = 427881078 AND NOT location_norm ~ 'OY|oy')
      -- Harmelingen
      AND NOT (osm_id = 915968323 AND NOT location_norm LIKE '%elingen%')
      -- Darmstadt-Mitte
      AND NOT (osm_id = 958260411 AND NOT location_norm LIKE '%armstadt%')
      -- Hull is not Hüll
      AND NOT (osm_id = 1418810642 AND NOT location_norm LIKE '%üll%')
      -- 'by the wall' is not Wall
      AND NOT (osm_id = 1700564762 AND location_norm ~ '[Tt]he')
      -- Deps
      AND NOT (osm_id = 475572472 AND location_norm LIKE '%ps%')
      -- Oedt
      AND NOT (osm_id = 475572472 AND location_norm LIKE '%edt%')
      -- Heimbach-Weis
      AND NOT (osm_id = 312870846 AND location_norm LIKE '%eis%')
      -- Sieber
      AND NOT (osm_id = 118509392 AND location_norm LIKE '%ieb%')
      -- Wehr
      AND NOT (osm_id = 5979620273 AND NOT location_norm LIKE '%ehr%')) OR

    -- HAMLETS, NEIGHBOURHOODS
    (place_rank = 20
      AND (type IN ('administrative', 'hamlet', 'neighbourhood', 'quarter',
                    'dock', 'waterfall') OR
           type = 'weir' AND osm_id IN (329996946, 216887132))
      AND location_norm ~ '[A-Za-z\u00c4\u00e4\u00d6\u00f6\u0400\u00dc\u00fc\u00df-\u04FF]{2,}'
      AND NOT location_norm ~ '^[^A-Za-z]*[Dd][Ee]([Uu][Tt][Ss][Cc][Hh][Ll][Aa][Nn][Dd])?[^A-Za-z]*$'
      AND NOT location_norm ~ '^[^A-Za-z]*[Gg][Ee][Rr]([Mm][Aa][Nn][Yy])?[^A-Za-z]*$'
      AND NOT osm_id IN (90160399, 91174284, 221301860, 267079183, 276774985,
                         281814044, 286036090, 293627964, 300311536, 335758747,
                         351907673, 357195052, 366063787, 370447407, 411605139,
                         427917448, 462115440, 465479489, 498752382, 499621982,
                         573634080, 618115251, 672882070, 693731913, 695056343,
                         705781235, 731235685, 747044047, 753135574, 760445870,
                         771729099, 781818208, 879077390, 879077617, 879078033,
                         886303609, 886576886, 946030538, 966809541, 1037988056,
                         1080154518, 1080287609, 1101490980, 1103603553, 1106948085,
                         1215762708, 1233324741, 1233400099, 1315196490, 1329565552,
                         1396407223, 1406128663, 1439652774, 1440171421, 1442067763,
                         1488272635, 1495723002, 1575569557, 1640516005, 1643937909,
                         1684545142, 1687505051, 1690966109, 1727084155, 1762934577,
                         1815579628, 1836742729, 1850621783, 1896163692, 1924097739,
                         1929403034, 2000307775, 2012557570, 2031516878, 2036818461,
                         2064871977, 2118893428, 2130417684, 2178651973, 2178651973,
                         2188182508, 2260283271, 2260323132, 2263384879, 2285461513,
                         2297454039, 2321780374, 2357314020, 2418868667, 2419561966,
                         2426994618, 2493587684, 2696688797, 2801769690, 2933545605,
                         3239958590, 3247621920, 3349561379, 3657736936, 3658518192,
                         3687909235, 3701681098, 3849876434, 3974202326, 4105818156,
                         4155985849, 4476685039, 5964093722, 6020069295, 6063109497,
                         6169348266, 6185958138, 6207109583, 6738031502, 6943377155,
                         7138885389, 7249450028, 7321344865, 7506627429, 7598400096,
                         7816868297, 7938893324, 8081452536, 8524969379, 9052184362,
                         357111, 1114214, 1136702, 2257202, 2613715, 3125216,
                         3548854, 3834813, 4718247, 7128541, 8005454, 8309339,
                         10958894, 13154599, 98196318, 106665599, 234838192,
                         781128616, 2594529431, 2015355408, 577173843, 286422632,
                         250355793, 83102235, 7778141)
      AND NOT osm_id BETWEEN 6185958138 AND 2963932895
      AND NOT osm_id BETWEEN 6292873585 AND 6298041199
      -- Märchen is not Marchen
      AND NOT (osm_id = 760445928 AND location_norm ~ '[Ää]')
      -- Walking
      AND NOT (osm_id = 879077562 AND location_norm ~ '[Tt]he')
      -- Wien, Traunstein
      AND NOT (osm_id = 1262215389 AND (NOT location_norm LIKE '%ien' OR
                                        location_norm LIKE '%&%'))
      -- Tannhäuser ~~Gate~~
      AND NOT (osm_id = 2249354152 AND location_norm LIKE '%Gate%')
      -- Finn is not Finna
      AND NOT (osm_id = 3711004950 AND NOT location_norm LIKE '%inna%')
      -- Paris is not Klein-Paris
      AND NOT (osm_id = 3817028270 AND NOT location_norm LIKE '%lein%')
      -- Berlin-Mitte, Wedding
      -- AND NOT (osm_id IN (16566, 28267) AND location_norm NOT LIKE '%erlin%')
      -- Hermsdorf
      AND NOT (osm_id = 55746 AND NOT location_norm LIKE '%ermsdorf%')
      -- Tiergarten
      AND NOT (osm_id = 55750 AND NOT location_norm LIKE '%iergarten%')
      -- Friedrichshain
      AND NOT (osm_id = 55763 AND NOT location_norm LIKE '%ain%')
      -- Kreuzberg
      AND NOT (osm_id = 55765 AND NOT location_norm ~ 'erg|ERG')
      -- Stegen
      AND NOT (osm_id = 74463 AND NOT location_norm LIKE '%tegen%')
      -- Hannover-Südstadt
      AND NOT (osm_id = 82728 AND NOT location_norm LIKE '%nover%')
      -- Düsseldorf
      AND NOT (osm_id BETWEEN 91145 AND 91147 AND NOT location_norm ~ 'lingern|seldorf')
      AND NOT (osm_id BETWEEN 91282 AND 92373 AND NOT location_norm LIKE '%seldorf%')
      AND NOT (osm_id = 93374 AND NOT location_norm LIKE '%ersten%')
      AND NOT (osm_id = 93375 AND NOT location_norm LIKE '%olt%')
      -- Lohausen
      AND NOT (osm_id = 93557 AND NOT location_norm LIKE '%oha%')
      -- Itter
      AND NOT (osm_id = 93381 AND NOT location_norm LIKE '%tter%')
      -- Stockum
      AND NOT (osm_id = 93545 AND NOT location_norm LIKE '%tockum%')
      -- Mahlsdorf
      AND NOT (osm_id = 164713 AND NOT location_norm LIKE '%ahls%')
      -- FIXME: Dortmund-Nordstadt is not Bonn Nordstadt
      AND NOT (osm_id = 272248 AND location_norm ~ '[Dd][Oo]')
      -- Gut Moor
      AND NOT (osm_id = 299481 AND NOT location_norm LIKE '%oor%')
      -- Gropiusstadt
      AND NOT (osm_id = 409215 AND NOT location_norm LIKE '%ropius%')
      -- Köln
      AND NOT (osm_id IN (2613717, 2613734, 2613773, 2613774, 2613775, 2613777) AND
               NOT location_norm LIKE '%öln%')
      -- Essen
      AND NOT (osm_id BETWEEN 3176320 AND 3176323 AND NOT location_norm LIKE '%ssen%')
      AND NOT (osm_id = 3189148 AND NOT location_norm LIKE '%reisen%')
      AND NOT (osm_id = 3190458 AND NOT location_norm LIKE '%orst%')
      -- Neckarstadt-Ost
      AND NOT (osm_id = 5192187 AND NOT location_norm LIKE '%eckar%')
      -- Meine
      AND NOT (osm_id = 7723965 AND NOT location_norm ~ '^[Mm][Ee][Ii][Nn][Ee](,.+)?$')
      -- Pries
      AND NOT (osm_id = 9950656 AND NOT location_norm LIKE '%ries%')
      -- Spork-Eichholz
      AND NOT (osm_id = 12422822 AND NOT location_norm LIKE '%hol%')
      -- Rheydt
      AND NOT (osm_id = 13154372 AND NOT location_norm LIKE '%hey%')
      -- Aufm Buch, Bayern
      AND NOT (osm_id = 410716676 AND NOT location_norm LIKE '%uch%')
      -- Wallsdorf
      AND NOT (osm_id = 301656013 AND NOT location_norm LIKE '%dorf%')
      -- Hammfeld
      AND NOT (osm_id = 8004301 AND NOT location_norm LIKE '%amm%')
      -- Hehn
      AND NOT (osm_id = 13154372 AND NOT location_norm LIKE '%ehn%')) OR

    -- POSTAL CODES
    (place_rank = 21
      AND type IN ('postal_code', 'postcode')
      AND location_norm ~ '\d{5}') OR

    -- CITY BLOCKS
    (place_rank = 22
      AND category = 'boundary'
      AND (importance BETWEEN 0.40 AND 0.442 OR
           importance BETWEEN 0.445 AND 0.472 OR
           importance BETWEEN 0.48 AND 0.543 OR
           importance BETWEEN 0.555 AND 0.64 OR
           importance > 0.645 OR
        -- Nauen
           location_norm LIKE '%auen%')
      AND NOT (location_norm ~ '\d' AND NOT location_norm ~ '\d{5}' OR
               NOT location_norm ~ '[A-Za-zäöüÄÖÜß]{3}')) OR

    (place_rank = 24
      AND type = 'administrative') OR

    (place_rank = 25
      AND type IN ('administrative', 'historic', 'historic:administrative',
                   'political', 'region', 'religious_administration')
      AND NOT (location_norm ~ '\d' AND NOT location_norm ~ '\d{5}' OR
               NOT location_norm ~ '[A-Za-zäöüÄÖÜß]{3}')
      AND NOT osm_id IN (3394110, 3394113, 3394115, 3810329)) OR

    (place_rank = 30
      AND importance > 0.1
      AND category = 'railway'
      AND NOT osm_id IN (5321226, 3895872989, 904363731, 218539828, 56696274,
                         73793044, 116975794, 137315642, 152533767, 195150312,
                         197684566, 203983200, 292457385, 362775661, 470241878,
                         1562853769, 1868965217, 2656117332, 3231396347, 3458182805,
                         5323532504, 6225424794)
      AND NOT (location_norm ~ '\d' AND NOT location_norm ~ '\d{5}' OR
               NOT location_norm ~ '[A-Za-zäöüÄÖÜß]{3}')
      -- Wuppertal
      AND NOT (osm_id = 5195474 AND NOT location_norm LIKE '%ertal%')
      -- Mülheim
      AND NOT (osm_id = 8375136 AND NOT location_norm LIKE '%heim%')
      -- Montabaur
      AND NOT (osm_id = 35539236 AND NOT location_norm LIKE '%onta%')
      -- Bützow
      AND NOT (osm_id = 42674387 AND NOT location_norm LIKE '%tzow%')
      -- Jünkerath
      AND NOT (osm_id = 63203931 AND NOT location_norm LIKE '%rath%')
      -- Brühl-Schwadorf
      AND NOT (osm_id = 104924219 AND NOT location_norm LIKE '%hl%')
      -- München
      AND NOT (osm_id = 157587585 AND NOT location_norm ~ 'ni?ch')
      -- Rommelshausen
      AND NOT (osm_id = 302014316 AND NOT location_norm LIKE '%hausen%')
      -- Stuttgart
      AND NOT (osm_id = 1635698272 AND NOT location_norm LIKE '%tgar%')
      -- Köln Süd, Töppeln
      AND NOT (osm_id IN (3044304327, 3059744036) AND NOT location_norm LIKE '%ln%')
      -- Karlsruhe
      AND NOT (osm_id = 2574283615 AND NOT location_norm LIKE '%arl%')
      -- Lehel
      AND NOT (osm_id = 2660339818 AND NOT location_norm LIKE '%ehe%')
      -- Kehl
      AND NOT (osm_id = 2931354552 AND NOT location_norm LIKE '%ehl%')
      -- Lette
      AND NOT (osm_id = 4168147467 AND NOT location_norm LIKE '%ett%')
      -- Duisburg-Buchholz
      AND NOT (osm_id = 3092340765 AND NOT location_norm LIKE '%burg%')
      -- Biberach
      AND NOT (osm_id = 3493107549 AND NOT location_norm LIKE '%rach%')
      -- Essen
      AND NOT (osm_id = 3493107549 AND NOT location_norm LIKE '%ssen%')
      -- Mainz-Bischofsheim
      AND NOT (osm_id = 4371485220 AND NOT location_norm LIKE '%ain%')
      -- Bottrop
      AND NOT (osm_id = 4733028656 AND NOT location_norm LIKE '%rop%')
      -- Eisenberg
      AND NOT (osm_id = 5113952519 AND NOT location_norm LIKE '%berg%')) OR

    -- Postal codes
    location_norm ~ '(?<!\d)\d{5}(?!\d)' OR

    -- Coordinates
    location_norm ~ '((?<![\d\.])\d{2}(° ?|\.)\d.+){2}'
  )
ORDER BY place_rank, osm_type, osm_id, location_norm;
